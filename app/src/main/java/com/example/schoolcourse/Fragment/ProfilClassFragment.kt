package com.example.schoolcourse.Fragment

import android.content.Context
import android.os.Bundle
import androidx.fragment.app.Fragment
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.schoolcourse.Adapter.AdapterRecyclerViewClass
import com.example.schoolcourse.Adapter.AdapterViewPagerClass
import com.example.schoolcourse.Model.ModelDataRecycleClass
import com.example.schoolcourse.R

class ProfilClassFragment : Fragment(R.layout.fragment_profil_class) {

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        val context : Context = requireActivity()

        val viewPager : ViewPager = view.findViewById(R.id.vpclass)
        val viewPager2 : ViewPager = view.findViewById(R.id.vpclass2)
        val listClass : RecyclerView = view.findViewById(R.id.rcclass)

        //set-View, Adapter & Data ViewPager
        var listVPup : ArrayList<Int> = ArrayList()
        listVPup.add(R.drawable.no_image)
        listVPup.add(R.drawable.no_image)

        val adapterViewP = AdapterViewPagerClass(context, listVPup)
        viewPager.adapter = adapterViewP

        var listVPclass : ArrayList<Int> = ArrayList()
        listVPclass.add(R.drawable.school)
        listVPclass.add(R.drawable.festivalmusic)
        listVPclass.add(R.drawable.school)
        listVPclass.add(R.drawable.festivalmusic)

        val adapterViewP2 = AdapterViewPagerClass(context, listVPclass)
        viewPager2.adapter = adapterViewP2

        //set-View, Adapter & Data RecyclerView
        var list: ArrayList<ModelDataRecycleClass> = ArrayList()
        list.add(ModelDataRecycleClass(
            "XII IPA",
            "1",
            25,
            "Yopa S., S.Kom."))
        list.add(ModelDataRecycleClass(
            "XII IPS",
            "2",
            20,
            "Kevin S., S.Kom."))

        val adapterRecyc = AdapterRecyclerViewClass(list)
        listClass.layoutManager = LinearLayoutManager(context,RecyclerView.VERTICAL,false)
        listClass.adapter = adapterRecyc


    }
}