package com.example.schoolcourse.Service

import com.example.schoolcourse.Model.ModelResultTask
import retrofit2.Call
import retrofit2.http.GET
import retrofit2.http.Query

interface ApiEndPoint {

    @GET("sampledata.php")
    fun getdata(@Query("NIS")num : Int?): Call<ArrayList<ModelResultTask>>
}