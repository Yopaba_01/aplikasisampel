package com.example.schoolcourse

import android.app.Activity
import android.content.Intent
import android.graphics.Bitmap
import android.media.Image
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.text.InputFilter
import android.text.Spanned
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.EditText
import android.widget.ImageButton
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.DialogFragment
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import com.example.schoolcourse.Adapter.AdapterRecyclerViewDiscussion
import com.example.schoolcourse.Model.ModelDataDiscussion

class DiscussionBoardActivity : AppCompatActivity() {

    private var listdis: ArrayList<ModelDataDiscussion> = ArrayList()
    var postEditText : EditText? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_discussion_board)
        setSupportActionBar(findViewById(R.id.tbdisboard))

        val actionbar = supportActionBar
        actionbar!!.title = "Discussion Board"
        actionbar.setDisplayHomeAsUpEnabled(true)
        val bitmap : Bitmap? = null

        listdis.add(ModelDataDiscussion("IPA XII","4HOUR","TEMA IPA",bitmap))
        listdis.add(ModelDataDiscussion("XII IPS","2Hour","TEMA IPS",bitmap))

        val listDiscussion : RecyclerView = findViewById(R.id.rv_itemdis)
        postEditText = findViewById(R.id.edtext1)

        var adapterRecyc = AdapterRecyclerViewDiscussion(this, listdis)
        listDiscussion.layoutManager = LinearLayoutManager(this, RecyclerView.VERTICAL,false)
        listDiscussion.adapter = adapterRecyc


        val btnUpload : Button = findViewById(R.id.upitemdis)
        btnUpload.setOnClickListener(View.OnClickListener {

            listdis.add(ModelDataDiscussion("IPA XII","4HOUR",postEditText?.text.toString(),bitmap))
            adapterRecyc.notifyDataSetChanged()
            postEditText?.text = null
        })

    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this,MainActivity::class.java)
        startActivity(intent)
        finish()
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    class CostumDialog : DialogFragment(){

        override fun onCreateView(inflater: LayoutInflater,
                                  container: ViewGroup?, savedInstanceState: Bundle?
        ): View? {
            dialog!!.window?.setBackgroundDrawableResource(R.drawable.bg_card_green)
            return inflater.inflate(R.layout.alert_dialog_pick_file,container,false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            val camera : Button = view.findViewById(R.id.btn_camera_dialog)
            val galery : Button = view.findViewById(R.id.btn_galery_dialog)
            val resultContract = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
                    result: ActivityResult? ->
                if (result?.resultCode == Activity.RESULT_OK){
                    Log.e("ImageSelect",""+result?.data?.extras?.get("data").toString())
                }else{
                    Log.e("ImageSelect","Data Kosong")
                }
            }

            camera.setOnClickListener(View.OnClickListener {
                var intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                resultContract.launch(intent)
//                Toast.makeText(view.context,"Aplikasi Tidak Suppurt",Toast.LENGTH_SHORT).show()
            }
            )

            galery.setOnClickListener {
                var intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                resultContract.launch(intent)

            }
        }

        override fun onStart() {
            super.onStart()
            val width = (resources.displayMetrics.widthPixels * 0.85).toInt()
            val heigth = (resources.displayMetrics.widthPixels * 0.40).toInt()
            dialog!!.window?.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
        }

    }
}
