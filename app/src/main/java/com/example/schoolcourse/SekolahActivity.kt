package com.example.schoolcourse

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.recyclerview.widget.GridLayoutManager
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.ViewPager
import com.example.schoolcourse.Adapter.*
import com.example.schoolcourse.Model.ModelDataButtonSekolah
import com.example.schoolcourse.Model.ModelDataJenjangSekolah
import com.example.schoolcourse.Model.ModelDataRelasiSekolah

class SekolahActivity : AppCompatActivity(),ViewPagerButtonOnClick {

    private var adapterVPJenjang : AdapterViewPagerJenjangSekolah? = null
    private lateinit var dataAll: ArrayList<ModelDataJenjangSekolah>
    private var dataSDNegeri: ArrayList<ModelDataJenjangSekolah> = ArrayList()
    private var dataSDSwasta: ArrayList<ModelDataJenjangSekolah> = ArrayList()
    private var dataSMPNegeri: ArrayList<ModelDataJenjangSekolah> = ArrayList()
    private var dataSMPSwasta: ArrayList<ModelDataJenjangSekolah> = ArrayList()
    private var dataSMANegeri: ArrayList<ModelDataJenjangSekolah> = ArrayList()
    private var dataSMASwasta: ArrayList<ModelDataJenjangSekolah> = ArrayList()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_sekolah)
        setSupportActionBar(findViewById(R.id.tbsekolah))

        val actionbar = supportActionBar
        actionbar!!.title = "Find Your School"
        actionbar.setDisplayHomeAsUpEnabled(true)


        val recycleview : RecyclerView = findViewById(R.id.rcbuttonsekolah)
        val vpJenjang : ViewPager = findViewById(R.id.vp_jenjangsekolah)
        val recyleviewrelasi : RecyclerView = findViewById(R.id.rc_relasisekolah)

        var dataMenu: ArrayList<ModelDataButtonSekolah> = ArrayList()
        dataMenu.add(
            ModelDataButtonSekolah(
                R.drawable.toga,
                "All"
            )
        )
        dataMenu.add(
            ModelDataButtonSekolah(
                R.drawable.sd,
                "SD Negeri"
            )
        )
        dataMenu.add(
            ModelDataButtonSekolah(
                R.drawable.sd,
                "SD Swasta"
            )
        )
        dataMenu.add(
            ModelDataButtonSekolah(
                R.drawable.smp,
                "SMP Negeri"
            )
        )
        dataMenu.add(
            ModelDataButtonSekolah(
                R.drawable.smp,
                "SMP Swasta"
            )
        )
        dataMenu.add(
            ModelDataButtonSekolah(
                R.drawable.sma,
                "SMA Negeri"
            )
        )
        dataMenu.add(
            ModelDataButtonSekolah(
                R.drawable.sma,
                "SMA Swasta"
            )
        )

        dataSDNegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SD Negeri A")
        )
        dataSDNegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SD Negeri B")
        )
        dataSDNegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SD Negeri C")
        )
        dataSDNegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SD Negeri D")
        )
        dataSDNegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SD Negeri E")
        )
        dataSDSwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SD Swasta A")
        )
        dataSDSwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SD Swasta B")
        )
        dataSDSwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SD Swasta C")
        )
        dataSDSwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SD Swasta D")
        )
        dataSDSwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SD Swasta E")
        )

        dataSMPNegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMP Negeri A")
        )
        dataSMPNegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMP Negeri B")
        )
        dataSMPNegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMP Negeri C")
        )
        dataSMPNegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMP Negeri D")
        )
        dataSMPNegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMP Negeri E")
        )
        dataSMPSwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMP Swasta A")
        )
        dataSMPSwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMP Swasta B")
        )
        dataSMPSwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMP Swasta C")
        )
        dataSMPSwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMP Swasta D")
        )
        dataSMPSwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMP Swasta E")
        )

        dataSMANegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMA Negeri A")
        )
        dataSMANegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMA Negeri B")
        )
        dataSMANegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMA Negeri C")
        )
        dataSMANegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMA Negeri D")
        )
        dataSMANegeri.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMA Negeri E")
        )
        dataSMASwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMA Swasta A")
        )
        dataSMASwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMA Swasta B")
        )
        dataSMASwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMA Swasta C")
        )
        dataSMASwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMA Swasta D")
        )
        dataSMASwasta.add(ModelDataJenjangSekolah(
            R.drawable.sekolah,
            "SMA Swasta E")
        )

        dataAll = ArrayList()
        getAllData(dataSDNegeri, dataSDSwasta, dataSMPNegeri,dataSMPSwasta, dataSMANegeri, dataSMASwasta)

        val adapter = AdapterRecycleViewButtonSekolah(dataMenu,this)
        recycleview.layoutManager = GridLayoutManager(this,1, GridLayoutManager.HORIZONTAL,false)
        recycleview.adapter = adapter

        adapterVPJenjang = AdapterViewPagerJenjangSekolah(this,dataAll,supportFragmentManager)
        vpJenjang.adapter = adapterVPJenjang

        var dataRelasiSekolah: ArrayList<ModelDataRelasiSekolah> = ArrayList()
        dataRelasiSekolah.add(
            ModelDataRelasiSekolah(
                R.drawable.no_image,
                "SD Negeri A",
                "Siswa 700",
                "Member"
            )
        )
        dataRelasiSekolah.add(
            ModelDataRelasiSekolah(
                R.drawable.no_image,
                "SD Swasta A",
                "Siswa 300",
                "Member"
            )
        )
        dataRelasiSekolah.add(
            ModelDataRelasiSekolah(
                R.drawable.no_image,
                "SMP Negeri A",
                "Siswa 950",
                "Member"
            )
        )
        dataRelasiSekolah.add(
            ModelDataRelasiSekolah(
                R.drawable.no_image,
                "SMP Swasta A",
                "Siswa 800",
                "Member"
            )
        )
        dataRelasiSekolah.add(
            ModelDataRelasiSekolah(
                R.drawable.no_image,
                "SMA Negeri A",
                "Siswa 1200",
                "Member"
            )
        )
        dataRelasiSekolah.add(
            ModelDataRelasiSekolah(
                R.drawable.no_image,
                "SMA Swasta A",
                "Siswa 900",
                "Member"
            )
        )
        dataRelasiSekolah.add(
            ModelDataRelasiSekolah(
                R.drawable.no_image,
                "SD Negeri B",
                "Siswa 700",
                "Member"
            )
        )
        val adapter1 = AdapterRecycleViewRelasiSekolah( dataRelasiSekolah,this)
        recyleviewrelasi.layoutManager = GridLayoutManager(this,1, GridLayoutManager.HORIZONTAL,false)
        recyleviewrelasi.adapter = adapter1
    }

    override fun onItemClick(posisi: Int) {
        dataAll = ArrayList()
        getAllData(dataSDNegeri, dataSDSwasta, dataSMPNegeri,dataSMPSwasta, dataSMANegeri, dataSMASwasta)
        Log.e("dataall",""+dataAll.toString())

        when (posisi) {
            1 -> adapterVPJenjang?.setNewData(dataAll)
            2 -> adapterVPJenjang?.setNewData(dataSDNegeri)
            3 -> adapterVPJenjang?.setNewData(dataSDSwasta)
            4 -> adapterVPJenjang?.setNewData(dataSMPNegeri)
            5 -> adapterVPJenjang?.setNewData(dataSMPNegeri)
            6 -> adapterVPJenjang?.setNewData(dataSMANegeri)
            7 -> adapterVPJenjang?.setNewData(dataSMASwasta)
            else ->{
                adapterVPJenjang?.setNewData(ArrayList())
            }
        }

    }

    private fun getAllData(data1: ArrayList<ModelDataJenjangSekolah>,
                           data2: ArrayList<ModelDataJenjangSekolah>,
                           data3: ArrayList<ModelDataJenjangSekolah>,
                           data4: ArrayList<ModelDataJenjangSekolah>,
                           data5: ArrayList<ModelDataJenjangSekolah>,
                           data6: ArrayList<ModelDataJenjangSekolah>){
        dataAll.addAll(data1)
        dataAll.addAll(data2)
        dataAll.addAll(data3)
        dataAll.addAll(data4)
        dataAll.addAll(data5)
        dataAll.addAll(data6)
    }

}