package com.example.schoolcourse.Controller

import android.content.Intent
import android.os.Bundle
import android.provider.MediaStore
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import androidx.fragment.app.DialogFragment
import com.example.schoolcourse.MainActivity
import com.example.schoolcourse.R

class JoinDialogSekolah : DialogFragment() {

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View? {
        dialog!!.window?.setBackgroundDrawableResource(R.color.white)
        return inflater.inflate(R.layout.alert_dialog_join,container,false)

    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val join : Button =view.findViewById(R.id.joinclass)
        val cancel : Button = view.findViewById(R.id.cancelkelas)

        join.setOnClickListener(View.OnClickListener {
            val intent = Intent(requireActivity(),MainActivity::class.java)
            startActivity(intent)
            requireActivity().finish()
        })

        cancel.setOnClickListener(View.OnClickListener {
            dismiss()
        })
    }

    override fun onStart() {
        super.onStart()
    }
}