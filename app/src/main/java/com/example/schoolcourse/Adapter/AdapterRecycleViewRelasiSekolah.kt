package com.example.schoolcourse.Adapter

import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import com.example.schoolcourse.Model.ModelDataRelasiSekolah
import com.example.schoolcourse.R

class AdapterRecycleViewRelasiSekolah (private val data: ArrayList<ModelDataRelasiSekolah>,
                                       private val onclik1: ViewPagerButtonOnClick) :
    RecyclerView.Adapter<AdapterRecycleViewRelasiSekolah.ViewHolder>(){

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {

        val txrelasinama : TextView
        val txrelasimurid : TextView
        val txrelasimember : TextView
        val imgrelasi : ImageView
        val cardView : CardView


        init {
            txrelasinama = view.findViewById(R.id.txrelasisekolah)
            txrelasimurid = view.findViewById(R.id.txrelasisekolah2)
            txrelasimember = view.findViewById(R.id.txrelasisekolah3)
            imgrelasi = view.findViewById(R.id.imgrelasisekolah)
            cardView = view.findViewById(R.id.cardrelasisekolah)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_relasi_sekolah, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val nama_relasi_sekolah: String = data.get(position).namasekolah
        val murid_relasi_sekolah: String = data.get(position).muridsekolah
        val member_relasi_sekolah: String = data.get(position).membersekolah
        val img_relasi_sekolah: Int = data.get(position).imagesekolah

        holder.txrelasinama.text = nama_relasi_sekolah
        holder.txrelasimurid.text = murid_relasi_sekolah
        holder.txrelasimember.text = member_relasi_sekolah
        holder.imgrelasi.setImageResource(img_relasi_sekolah)

        holder.cardView.setOnClickListener(View.OnClickListener {
            onclik1.onItemClick(position+1)
            Log.e("posisi",""+(position+1))
        })
    }

    override fun getItemCount(): Int {
        return data.size
    }
}