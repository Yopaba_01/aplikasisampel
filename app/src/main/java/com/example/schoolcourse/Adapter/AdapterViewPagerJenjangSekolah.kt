package com.example.schoolcourse.Adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.constraintlayout.widget.ConstraintLayout
import androidx.fragment.app.FragmentManager
import androidx.viewpager.widget.PagerAdapter
import com.example.schoolcourse.Controller.JoinDialogSekolah
import com.example.schoolcourse.Model.ModelDataJenjangSekolah
import com.example.schoolcourse.R

class AdapterViewPagerJenjangSekolah(var context: Context,
                                     var data: ArrayList<ModelDataJenjangSekolah>,
                                     var fragmentManager: FragmentManager)
    : PagerAdapter() {
    private lateinit var layoutInflater : LayoutInflater
    private lateinit var consLayout : ConstraintLayout

    override fun getCount(): Int {
        return data.size
    }

    override fun isViewFromObject(view: View, `object`: Any): Boolean {
        return view.equals(`object`)
    }

    override fun instantiateItem(container: ViewGroup, position: Int): Any {
        layoutInflater = LayoutInflater.from(context)
        var view = layoutInflater.inflate(R.layout.item_jenjang_sekolah,container,false)

        var nameClass : TextView = view.findViewById<TextView>(R.id.txsekolah)
        var imageClass : ImageView = view.findViewById<ImageView>(R.id.img_jenjang)
        consLayout = view.findViewById(R.id.item_skolah_conslayout)

        container.addView(view,0)

        if (!data.isEmpty()){
            nameClass.text = data.get(position).namasekolah
            imageClass.setImageResource(data.get(position).imagesekolah)
        }else{
            nameClass.text = "Sekolah belum terdaftar"
            imageClass.setImageResource(R.drawable.no_image)
        }

        consLayout.setOnClickListener(View.OnClickListener {
            JoinDialogSekolah().show(fragmentManager,"JOIN")
        })

        return view
    }

    override fun destroyItem(container: ViewGroup, position: Int, `object`: Any) {
        container.removeView(`object` as View)
    }

    public fun setNewData( da: ArrayList<ModelDataJenjangSekolah>){
        data.clear()
        data.addAll(da)
        Log.e("data",""+data.toString())
        notifyDataSetChanged()
    }
}