package com.example.schoolcourse.Adapter

import android.content.Context
import android.graphics.Bitmap
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.schoolcourse.Model.ModelDataDiscussion
import com.example.schoolcourse.R

class AdapterRecyclerViewDiscussion (private val con: Context, private val data: ArrayList<ModelDataDiscussion>) :
    RecyclerView.Adapter<AdapterRecyclerViewDiscussion.ViewHolder>(){

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {

        val txJudulpos : TextView
        val txtimepos : TextView
        val txisipos : TextView
        val imgpos : ImageView

        init {
            txJudulpos = view.findViewById(R.id.txnamaitemdis)
            txtimepos = view.findViewById(R.id.txjamitem)
            txisipos = view.findViewById(R.id.txitempost)
            imgpos = view.findViewById(R.id.imgitemdis)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_discuccion_board, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val judul_discussion : String = data.get(position).disjudul
        val waktu_post : String = data.get(position).timepos
        val isi_post : String = data.get(position).posti
        val foto_post : Bitmap? = data.get(position).ppost

        holder.txJudulpos.text = judul_discussion
        holder.txtimepos.text = waktu_post
        holder.txisipos.text = isi_post
        holder.imgpos.setImageBitmap(foto_post)

    }

    override fun getItemCount(): Int {
        return data.size
    }

    public fun setData(newData: ArrayList<ModelDataDiscussion>){
        data.clear()
        data.addAll(newData)
        Log.e("data",""+data.toString())
        notifyDataSetChanged()
    }
}