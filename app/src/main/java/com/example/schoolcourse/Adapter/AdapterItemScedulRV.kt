package com.example.schoolcourse.Adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import com.example.schoolcourse.Model.ModelScedule
import com.example.schoolcourse.R

class AdapterItemScedulRV(private val data : ArrayList<ModelScedule>)
    : RecyclerView.Adapter<AdapterItemScedulRV.ViewHolder>() {

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {
        val txMapel1 : TextView
        val txTime1 : TextView
        val txMapel2 : TextView
        val txTime2 : TextView
        val txMapel3 : TextView
        val txTime3 : TextView
        val txMapel4 : TextView
        val txTime4 : TextView
        init {
            txMapel1 = view.findViewById(R.id.tx_mapel1)
            txTime1 = view.findViewById(R.id.tx_mapel_jadwal1)
            txMapel2 = view.findViewById(R.id.tx_mapel2)
            txTime2 = view.findViewById(R.id.tx_mapel_jadwal2)
            txMapel3 = view.findViewById(R.id.tx_mapel3)
            txTime3 = view.findViewById(R.id.tx_mapel_jadwal3)
            txMapel4 = view.findViewById(R.id.tx_mapel4)
            txTime4 = view.findViewById(R.id.tx_mapel_jadwal4)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_scedule, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        holder.txMapel1.text = data.get(position).mapel1
        holder.txTime1.text = data.get(position).time1
        holder.txMapel2.text = data.get(position).mapel2
        holder.txTime2.text = data.get(position).time2
        holder.txMapel3.text = data.get(position).mapel3
        holder.txTime3.text = data.get(position).time3
        holder.txMapel4.text = data.get(position).mapel4
        holder.txTime4.text = data.get(position).time4
    }

    override fun getItemCount()=data.size
}