package com.example.schoolcourse.Adapter

import android.content.Context
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.RelativeLayout
import android.widget.TextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import androidx.viewpager.widget.PagerAdapter
import com.example.schoolcourse.R
import com.example.schoolcourse.Model.ModelDataButtonSekolah

class AdapterRecycleViewButtonSekolah (private val data: ArrayList<ModelDataButtonSekolah>,
                                       private val onclik1: ViewPagerButtonOnClick) :
    RecyclerView.Adapter<AdapterRecycleViewButtonSekolah.ViewHolder>(){

    class ViewHolder(view : View) : RecyclerView.ViewHolder(view) {

        val txbuttonsekolah : TextView
        val img : ImageView
        val cardView : CardView


        init {
            txbuttonsekolah = view.findViewById(R.id.txbuttonsekolah)
            img = view.findViewById(R.id.imageSd)
            cardView = view.findViewById(R.id.cvbuttonsekolah)
        }

    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val view = LayoutInflater.from(parent.context)
            .inflate(R.layout.item_button_sekolah, parent, false)

        return ViewHolder(view)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val nama_sekolah: String = data.get(position).namasekolah
        val img_sekolah: Int = data.get(position).imagesekolah

        holder.txbuttonsekolah.text = nama_sekolah
        holder.img.setImageResource(img_sekolah)

        holder.cardView.setOnClickListener(View.OnClickListener {
            onclik1.onItemClick(position+1)
            Log.e("posisi",""+(position+1))
        })
    }

    override fun getItemCount(): Int {
        return data.size
    }
}