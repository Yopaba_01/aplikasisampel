package com.example.schoolcourse.Model

class ModelScedule {

    var mapel1 : String = "Judul Task"
        get() = field
        set(value) {field=value}

    var time1 : String = "00:00"
        get() = field
        set(value) {field=value}

    var mapel2 : String = "Judul Task"
        get() = field
        set(value) {field=value}

    var time2 : String = "00:00"
        get() = field
        set(value) {field=value}

    var mapel3 : String = "Judul Task"
        get() = field
        set(value) {field=value}

    var time3 : String = "00:00"
        get() = field
        set(value) {field=value}

    var mapel4 : String = "Judul Task"
        get() = field
        set(value) {field=value}

    var time4 : String = "00:00"
        get() = field
        set(value) {field=value}

    constructor(
        mapel1: String,
        time1: String,
        mapel2: String,
        time2: String,
        mapel3: String,
        time3: String,
        mapel4: String,
        time4: String
    ) {
        this.mapel1 = mapel1
        this.time1 = time1
        this.mapel2 = mapel2
        this.time2 = time2
        this.mapel3 = mapel3
        this.time3 = time3
        this.mapel4 = mapel4
        this.time4 = time4
    }


}