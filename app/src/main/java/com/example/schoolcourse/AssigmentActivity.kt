package com.example.schoolcourse

import android.app.Activity
import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.provider.MediaStore
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Button
import android.widget.ImageView
import android.widget.TextView
import android.widget.Toast
import androidx.activity.result.ActivityResult
import androidx.activity.result.contract.ActivityResultContracts
import androidx.fragment.app.DialogFragment
import com.example.schoolcourse.Model.ModelResultTask
import com.example.schoolcourse.Service.ApiServices
import retrofit2.Call
import retrofit2.Callback


class AssigmentActivity : AppCompatActivity() {

    lateinit var image : ImageView
    lateinit var nis : TextView
    lateinit var nama : TextView
    lateinit var kelas : TextView
    lateinit var soal : TextView
    private val dataList = ArrayList<ModelResultTask>()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_assigment)
        setSupportActionBar(findViewById(R.id.tbasigmt))


        val actionbar = supportActionBar
        actionbar!!.title = "Assigment"
        actionbar.setDisplayHomeAsUpEnabled(true)

        image = findViewById(R.id.tesimage)
        nis = findViewById(R.id.txnis2)
        nama = findViewById(R.id.txname2)
        kelas = findViewById(R.id.txkelas2)
        soal = findViewById(R.id.txsoal)

        val btnAss : Button = findViewById(R.id.btnasig)
        btnAss.setOnClickListener(View.OnClickListener {

            CostumDialog().show(supportFragmentManager,"SELECT CONTENT")
        })

    }

    override fun onStart() {
        super.onStart()
        var pos = intent.extras?.getInt("POSISI")
        Log.e("POSISI",""+pos.toString())
        ambilSoal(pos)
    }

    private fun ambilSoal(nomer : Int?){
        ApiServices.instance.getdata(nomer).enqueue(object : Callback<ArrayList<ModelResultTask>>{
            override fun onResponse(
                call: Call<ArrayList<ModelResultTask>>,
                response: retrofit2.Response<ArrayList<ModelResultTask>>
            ) {
                Log.e("DATA",""+response.body()?.toString())
                response.body()?.let { dataList.addAll(it)}
                nis.text = dataList.get(0).NIS
                nama.text = dataList.get(0).Nama
                kelas.text = dataList.get(0).Kelas
                soal.text = dataList.get(0).Soal
                Log.e("DATA SOAL",""+dataList.toString())
                Log.e("CODE",""+response.code().toString())
            }

            override fun onFailure(call: Call<ArrayList<ModelResultTask>>, t: Throwable) {
                Toast.makeText(this@AssigmentActivity, ""+t.toString(), Toast.LENGTH_SHORT).show()
                Log.e("onFailure",""+t.toString())
            }
        })
    }

    override fun onBackPressed() {
        super.onBackPressed()
        val intent = Intent(this, TaskHistoryActivity::class.java)
        startActivity(intent)
    }

    override fun onSupportNavigateUp(): Boolean {
        onBackPressed()
        return true
    }

    class CostumDialog : DialogFragment(){

        override fun onCreateView(inflater: LayoutInflater,
                                  container: ViewGroup?, savedInstanceState: Bundle?
        ): View? {
            dialog!!.window?.setBackgroundDrawableResource(R.drawable.bg_card_green)
            return inflater.inflate(R.layout.alert_dialog_pick_file,container,false)
        }

        override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
            super.onViewCreated(view, savedInstanceState)
            val camera : Button = view.findViewById(R.id.btn_camera_dialog)
            val galery : Button = view.findViewById(R.id.btn_galery_dialog)
            val resultContract = registerForActivityResult(ActivityResultContracts.StartActivityForResult()){
                    result: ActivityResult? ->
                if (result?.resultCode == Activity.RESULT_OK){
                    Log.e("ImageSelect",""+result?.data?.extras?.get("data").toString())
                }else{
                    Log.e("ImageSelect","Data Kosong")
                }
            }

            camera.setOnClickListener(View.OnClickListener {
                var intent = Intent(MediaStore.ACTION_IMAGE_CAPTURE)
                resultContract.launch(intent)
//                Toast.makeText(view.context,"Aplikasi Tidak Suppurt",Toast.LENGTH_SHORT).show()
            }
            )

            galery.setOnClickListener {
                var intent = Intent(Intent.ACTION_PICK, MediaStore.Images.Media.INTERNAL_CONTENT_URI)
                resultContract.launch(intent)

            }
        }

        override fun onStart() {
            super.onStart()
            val width = (resources.displayMetrics.widthPixels * 0.85).toInt()
            val heigth = (resources.displayMetrics.widthPixels * 0.40).toInt()
            dialog!!.window?.setLayout(width, ViewGroup.LayoutParams.WRAP_CONTENT)
        }
    }
}